## Advanced Gutenberg Blocks
## Description

Custom Gutenberg Blocks for Site Builders and Editors. Create Better Content With Gutenberg.

Advanced Gutenberg Blocks is a Gutenberg blocks module for site builder and editors. Our goal is to make it insanely easy for you to create better and engaging content with Gutenberg.

## Advanced Gutenberg Blocks currently includes the following blocks:

* Section Break (H2)
* Custom Heading
* Heighlighted Text
* Image Grid 
* Custom Text Block 
* Text + Image Slider - Repeater
* Featured Media 
* Image Mosaic
* Media Repeater Grid
* Link Mosaic
* Featured Text
* Text + Image 
* Call to Action
* Link List
* Text Mosaic
* Full width Image Slider
* Spacer Block
* FAQ Accordion
* Custom Audio Block

We have more exciting blocks in the making. Have a suggestion? [Let us know](https://www.skynettechnologies.com/contact-us).

## MAINTAINERS

Current maintainers:
 * Rajesh Bhimani (Skynet Technologies) - https://www.drupal.org/user/3479403


This project has been sponsored by:
 * Skynet technologies
   Skynet Technologies is ISO 9001:2015 & 27001:2013 certified company providing end-to-end IT Services including Website Design & Development, ecommerce shopping cart, SEO & Digital Marketing, ADA Website Design, Custom Analytics, Mobile App, CRM, ERP & Custom Software Development for 20 years. We deliver our services to Start-Ups, SMEs, Corporates, Government & Agencies.

   Our headquarter is in Amelia, Ohio. Also, We have subsidiaries in the USA (Nevada, Florida), India, and Australia. We are providing solutions for small to large businesses, enterprises, corporations, web development agencies, and firms. We believe in the customer-first approach. We go above and beyond to meet the tailored needs of our customers. Our primary aim is to serve our customers by allowing them to do a successful and profitable business with us online.
   Visit https://www.skynettechnologies.com for more information.
