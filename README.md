# Advanced Gutenberg Blocks

Custom Gutenberg Blocks for Site Builders and Editors.
Create Better Content With Gutenberg.

Advanced Gutenberg Blocks is a Gutenberg blocks module for
site builder and editors. Our goal is to make it insanely easy for
you to create better and engaging content with Gutenberg.

## Installation
### Prerequisites
- Drupal 8.x, 9.X, 10.X or 11.X
  
### Steps
You can install this module with two ways.

#### Via Composer
    1.  Navigate to your Drupal root directory.
    2.  Run the following command to install the module:
        ``` bash
        composer require drupal/advanced_gutenberg_blocks  
        ```     
    3. After the module is installed, enable it using
   Drush or through the Drupal admin interface.

#### Manual Installation
    1. Download the module from [Drupal.org](https://www.drupal.org/project/advanced_gutenberg_blocks).
    2. Extract the downloaded file and place it in the 
   `modules/contrib` directory of your Drupal installation.
    3. Enable the module using Drush or through the Drupal admin interface.

## Advanced Gutenberg Blocks currently includes the following blocks:

* Section Break (H2)
* Custom Heading
* Heighlighted Text
* Image Grid 
* Custom Text Block 
* Text + Image Slider - Repeater
* Featured Media 
* Image Mosaic
* Media Reapeter Grid
* Link Mosaic
* Featured Text
* Text + Image 
* Call to Action
* Link List
* Text Mosaic
* Full width Image Slider
* Spacer Block
* FAQ Accordion
* Custom Audio Block
  
## Configuration

- You need to create or edit content type to use advanced gutenberg blocks.

## Send Us an Email
Alternatively, you can send an email to our support team:
[hello@skynettechnologies.com](mailto:hello@skynettechnologies.com)

## Credits
This addon is developed and maintained by [Skynet Technologies USA LLC](https://www.skynettechnologies.com)

## Current Maintainers
- [Skynet Technologies USA LLC](https://www.drupal.org/skynet-technologies-usa-llc)
