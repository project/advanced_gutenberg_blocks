const { blocks, data, element, components, editor } = wp;
const { registerBlockType } = blocks;
const { dispatch, select } = data;
const { Fragment, useState } = element;
const { PanelBody, BaseControl, Icon, RangeControl, IconButton, Toolbar, SelectControl, CheckboxControl } = components;
const { InnerBlocks, RichText, InspectorControls, PanelColorSettings, MediaUpload, BlockControls } = editor;
const __ = Drupal.t;

// Test Block
const sectionBreakH2 = {
    title: __('Section Break (H2) Demo'),
    description: __('H2 used for Section Heading'),
    attributes: {
        title: {
            type: 'string',
        },
       
        backgroundflag: {
          type: 'string',
        },
    },
  
    
  
    edit({ attributes, setAttributes, isSelected }) {
        const { title, backgroundflag } = attributes;
        return (
          <Fragment>
            
            <RichText
                  identifier="title"
                  tagName="h2"
                  value={title}
                  placeholder={__('Section Break (H2)')}
                  onChange={title => {
                      setAttributes({
                          title: title,
                      });
                  }}
                  onSplit={() => null}
                  unstableOnSplit={() => null}
              />
               
            <InspectorControls>
            <PanelBody title={ __('Block Settings') }>
              
              
              <SelectControl
              label="Background Color"
              value={ backgroundflag }
              options={ [
                  { label: 'White', value: 'white_background' },
                  { label: 'Grey', value: 'grey_background' },
                  { label: 'Red', value: 'red_background' },
              ] }
              onChange={backgroundflag => {
                  setAttributes({
                    backgroundflag: backgroundflag,
                  });
              }}
            />
            </PanelBody>
            
          </InspectorControls>
          </Fragment>
        );
    },
  
    save({ attributes }) {
      const { title, backgroundflag} = attributes;
      
      return (
        <div className={`${(typeof backgroundflag!==undefined) ? backgroundflag : "white_background"}`}>
          <div className="sectionBreakH2_wrapper">
            {title && (<h2>{title}</h2>)}
          </div>
        </div>
      );
    },
  };
  
  const category = {
    slug: 'example',
    title: __('Custom Body Modules'),
  };
  
  const currentCategories = select('core/blocks').getCategories().filter(item => item.slug !== category.slug);
  dispatch('core/blocks').setCategories([ category, ...currentCategories ]);
  
  registerBlockType(`${category.slug}/sectionbreakdemo-block`, { category: category.slug, ...sectionBreakH2 });